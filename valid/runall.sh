#!/bin/bash

function rundisksim()
{
    count=100
    while [[ $count -ge 25 ]];
        do
        count=$((count-5))
        ratio=0.$count
        echo $ratio
        ./disksim  Seagate-Cheetah15k5.parvr${read}loc${local}  out.seagater${read}loc${local}ratio$ratio  ascii  0 1 $ratio
        mv out.seagater${read}loc${local}ratio$ratio r${read}loc${local}/
    done
    ./disksim  Seagate-Cheetah15k5.parvr${read}loc${local}  out.seagater${read}loc${local}ratio1.00  ascii  0 1 1.00
    mv out.seagater${read}loc${local}ratio1.00 r${read}loc${local}/
}

read=0.1
local=0.1
for r in 95 80 65 50 35 20 05
do
    read=0.${r}
    for l in 95 80 65 50 35 20 05 
    do
        local=0.${l}
        echo read $read   local $local
        mkdir r${read}loc${local}
        rundisksim
    done
done
